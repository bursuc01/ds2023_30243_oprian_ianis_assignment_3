export interface Device {
    id: number;
    description: string;
    address: string;
    maximumHourlyEnergyConsumption: number;
    userId: number;
  }