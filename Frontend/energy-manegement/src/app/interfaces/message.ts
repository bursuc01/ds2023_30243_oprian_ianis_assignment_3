export interface Message {
    content: string;
    sender: string;
    receiver: string;
  }