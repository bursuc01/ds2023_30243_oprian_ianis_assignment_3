namespace Site.DataLayer.DTO;

public class AuthenticatedResponseDTO
{
    public string? Token { get; set; }
}