namespace SiteUser.DataLayer.DTO;

public class UserFrontendDTO
{ 
        public string? Name { get; set; } 
        public bool? IsAdmin { get; set; }
}